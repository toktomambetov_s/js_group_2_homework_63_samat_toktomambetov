import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://homework-63-87de8.firebaseio.com/'
});

export default instance;