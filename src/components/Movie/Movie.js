import React, { PureComponent } from 'react';

class Movie extends PureComponent {
    render() {
        return (
            <div className="Movie">
                <input type="text" onChange={this.props.change} value={this.props.name}/> <button className="Remove" onClick={this.props.remove}>X</button>
            </div>
        )
    }
};

export default Movie;