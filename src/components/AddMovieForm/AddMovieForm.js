import React from 'react';

const AddMovieForm = props => {
    return (
        <div className="AddMovieForm">
            <label htmlFor="">Movie name: </label>
            <input type="text" onChange={props.change} value={props.currentMovieName}/>
            <button className="Add" onClick={props.click}>Add</button>
        </div>
    )
};

export default AddMovieForm;