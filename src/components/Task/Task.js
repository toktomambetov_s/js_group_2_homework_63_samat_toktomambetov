import React from 'react';

const Task = props => {
    return (
      <div className="Task">
          <p>{props.name} <button className="Remove" onClick={props.remove}>X</button></p>
      </div>
    );
};

export default Task;