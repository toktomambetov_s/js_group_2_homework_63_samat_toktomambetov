import React from 'react';

const AddTaskForm = props => {
    return (
      <div className="AddTaskForm">
          <label htmlFor="">Task name: </label>
          <input type="text" onChange={props.taskName} value={props.currentTaskName}/>
          <button className="Add" onClick={props.click}>Add</button>
      </div>
    );
};

export default AddTaskForm;