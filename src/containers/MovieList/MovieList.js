import React, { Component, Fragment } from 'react';
import AddMovieForm from "../../components/AddMovieForm/AddMovieForm";
import Movie from "../../components/Movie/Movie";
import Spinner from "../../components/UI/Spinner/Spinner";

import axios from '../../axios-movies';

class MovieList extends Component {
    state = {
        movies: [],
        currentMovie: '',
        loading: false
    };

    changeMovieName = e => this.setState({currentMovie: e.target.value});

    componentDidMount() {
        axios.get('/movies.json').then(response => {
            console.log(response);
            const movies = [];
            for (let key in response.data) {
                movies.push({...response.data[key], id: key});
            }

            this.setState({movies: movies});
        })
    };

    addMovie = (event) => {
        event.preventDefault();
        this.setState({loading: true});
        const movie = {
            name: this.state.currentMovie
        };

        axios.post('/movies.json', movie).finally(() => {
            this.setState({loading: false});
            this.props.history.push('/movie-list');
        });
    };

    removeMovie = (id, index) => {
        console.log(id);
        axios.delete('/movies/' + id + '.json').finally(() => {
            this.props.history.push('/movie-list');
        });

        const movies = [...this.state.movies];
        movies.splice(index, 1);
        this.setState({movies});
    };

    editMovieName = (e, index) => {
        console.log(e, index);
      const movies = [...this.state.movies];
      const movie = {...movies[index]};
      movie.name = e.target.value;
      movies[index] = movie;
      this.setState({movies});
    };

    render() {
        let form = (
            <div>
                {this.state.movies.map((movie, index) => (
                    <Movie change={(e) => this.editMovieName(e, index)} key={index} name={movie.name} remove={() => this.removeMovie(movie.id, index)}/>
                ))}
            </div>
        );

        if (this.state.loading) {
            form = <Spinner/>
        }

        return (
            <Fragment>
                <AddMovieForm change={this.changeMovieName} currentMovieName={this.state.currentMovie} click={this.addMovie}/>
                <h3>To watch list: </h3>
                {form}
            </Fragment>
        );
    }
}

export default MovieList;