import React, { Component, Fragment } from 'react';
import AddTaskForm from "../../components/AddTaskForm/AddTaskForm";
import Task from "../../components/Task/Task";
import Spinner from "../../components/UI/Spinner/Spinner";

import axios from '../../axios-tasks';

class ToDoList extends Component {
    state ={
        tasks: [],
        currentTask: '',
        loading: false
    };

    changeTaskName = e => this.setState({currentTask: e.target.value});

    componentDidMount() {
        axios.get('/tasks.json').then(response => {
            console.log(response);
            const tasks = [];
            for (let key in response.data) {
                tasks.push({...response.data[key], id: key});
            }

            this.setState({tasks: tasks});
        })
    }

    addTask = (event) => {
        event.preventDefault();
        this.setState({loading: true});
        const task = {
            name: this.state.currentTask
        };

        axios.post('/tasks.json', task).finally(() => {
            this.setState({loading: false});
            this.props.history.push('/todo-list');
        });
    };

    removeTask = (id, index) => {
        console.log(id);
        axios.delete('/tasks/' + id + '.json').finally(() => {
            this.props.history.push('/todo-list');
        });

        const tasks = [...this.state.tasks];
        tasks.splice(index, 1);
        this.setState({tasks});
    };

    render() {
        let form = (
            <div>
                {this.state.tasks.map((task, index) => (
                    <Task name={task.name} remove={() => this.removeTask(task.id, index)}/>
                ))}
            </div>
        );

        if (this.state.loading) {
            form = <Spinner />
        }

        return (
            <Fragment>
                <AddTaskForm taskName={this.changeTaskName} currentTaskName={this.state.currentTask} click={this.addTask}/>
                {form}
            </Fragment>
        )
    }
}

export default ToDoList;