import React, { Component, Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import Navbar from "./components/Navbar/Navbar";
import ToDoList from "./containers/ToDoList/ToDoList";
import MovieList from "./containers/MovieList/MovieList";

class App extends Component {
  render() {
    return (
        <Fragment>
          <Navbar />

          <Switch>
            <Route path="/" exact component={ToDoList} />
              <Route path="/todo-list" exact component={ToDoList}/>
            <Route path="/movie-list" exact component={MovieList}/>
          </Switch>
        </Fragment>
    );
  }
}

export default App;
